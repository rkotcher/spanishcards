var autoprefixer = require('autoprefixer');
var path = require('path');

var browserify = {
  'browser': {
    src: 'browser/index.jsx',
    dest: 'browser/dist/index.js',
    options: {
      transform: [['babelify', {
        presets: ['react', 'es2015'],
        plugins: ['syntax-object-rest-spread'],
      }]],
      browserifyOptions: {
        extensions: ['.jsx'],
        paths: [path.resolve()],
        debug: false,
      },
      keepAlive: false,
    },
  }
};

var concurrent = {
  'browser': {
    tasks: [
      'watch:browser_js',
      'watch:browser_less'
    ],
    options: { logConcurrentOutput: true },
  }
};

var copy = {
  all: {
    expand: true,
    cwd: 'browser/',
    dest: 'browser/dist/',
    src: [
      '**/*.html',
      '**/*.css',
      '**/*.eot',
      '**/*.woff',
      '**/*.woff2',
      '**/*.ttf',
      '**/*.svg',
      '**/*.otf',
      '**/*.png',
      '**/*.ico',
      '**/*.gif',
    ],
  }
};

var exec = {
  makeDist: 'rm -f -r browser/dist && mkdir -p browser/dist',
  copyIndex: 'cp browser/index.html browser/dist/index.html',
  copyVendor: 'cp browser/vendor/* browser/dist',
};

var less = {
  'browser': {
    options: {
      sourceMap: true,
      outputSourceFiles: true,
      paths: [path.resolve()],
    },
    files: {
      'browser/dist/main.css': 'browser/main.less',
    },
  }
};

var postcss = {
  'options': {
    map: true,
    processors: [ autoprefixer({ browsers: ['> 1%'] }) ],
  },
  'browser': {
    src: 'browser/dist/main.css',
    dest: 'browser/dist/main.postcss.css',
  }
}

var watch = {
  'browser_js': {
    atBegin: true,
    files: ['browser/**/*.jsx'],
    tasks: ['browserify:browser'],
  },
  'browser_less': {
    atBegin: true,
    files: ['browser/**/*.less'],
    tasks: ['less:browser', 'postcss:browser'],
  }
};

module.exports = function(grunt) {
  grunt.initConfig({
    browserify: browserify,
    concurrent: concurrent,
    copy: copy,
    less: less,
    exec: exec,
    postcss: postcss,
    watch: watch,
  });

  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-exec');
  grunt.loadNpmTasks('grunt-postcss');

  grunt.registerTask('build:browser', [
    'exec:makeDist',
    'copy:all',
    'exec:copyIndex',
    'exec:copyVendor',
    'less:browser',
    'postcss:browser',
    'browserify:browser'
  ]);

  grunt.registerTask('monitor:browser', [
    'build:browser',
    'concurrent:browser'
  ]);
};

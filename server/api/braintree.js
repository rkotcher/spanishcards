import braintree from 'braintree';
import express from 'express';
import User from '../User';

const router = express.Router();

const config = require(`../config.${ process.env.NODE_ENV }.js`);

const gateway = braintree.connect({
  environment: config.BRAINTREE_ENV,
  merchantId: config.BRAINTREE_MERCHANT_ID,
  publicKey: config.BRAINTREE_PUBLIC_KEY,
  privateKey: config.BRAINTREE_PRIVATE_KEY,
});

/*
 *
 * This is called when a user is created on the register page,
 * after successful call to register function
 *
**/
router.post('/customer', function(req, res, next) {
  if (!req.session.userId) {
    return res.status(500).json({ message: 'You must log in first.' });
  }

  const params = {
    '_id': req.session.userId,
  };
  User.findOne(params, (err, user) => {
    if (err) return res.json({ '_id': null });

    const data = {
      'id': req.session.userId,
      'firstName': user.displayName,
      'email': user.email,
    };
    gateway.customer.create(data, (err, data) => {
      if (err) return res.status(500).json(err);
      if (!data.success) return res.status(500).json(data);

      res.json(data);
    });
  });
});

/*
 *
 * Be sure to create a customer via POST /api/braintree/customer first,
 * as client tokens must be associated with user's vault
 *
**/
router.get('/client_token', function(req, res, next) {
  if (!req.session.userId) {
    return res.status(500).json({ message: 'You must log in first.' });
  }

  gateway.clientToken.generate({ 'customerId': req.session.userId},
      (err, data) => {
    res.send(data.clientToken);
  });
});

/*
 *
 *  create a subscription from this users payment_method_token
 *
 **/
router.post('/subscription', function(req, res, next) {
  if (!req.session.userId) {
    return res.status(500).json({ message: 'You must log in first.' });
  }

  const options = {
    paymentMethodNonce: req.body.payment_method_nonce,
    planId: '46jm',
  };
  gateway.subscription.create(options, (err, data) => {
    if (!data.success) {
      return res.status(500).json({ 'message': 'Error creating subscription' });
    }

    const params = {
      '_id': req.session.userId,
    };
    User.findOne(params, (err, user) => {
      user.subscriptionId = data.subscription.id;
      user.save(err => {
        if (err) return res.status(500).json({});
        res.status(200).json(data.subscription);
      });
    });
  });
});

router.get('/subscription', function(req, res, next) {
  if (!req.session.userId) {
    return res.status(500).json({ message: 'You must log in first.' });
  }

  const params = {
    '_id': req.session.userId,
  };
  User.findOne(params, (err, user) => {
    if (!user.subscriptionId) {
      return res.json({ subscription: null })
    }

    gateway.subscription.find(user.subscriptionId, (err, subscription) => {
      if (err) {
        return res.status(500).json({ 'message': 'Error fetching subscription' });
      }

      res.json({ subscription: subscription });
    })
  });
});

router.delete('/subscription', function(req, res, next) {
  if (!req.session.userId) {
    return res.status(500).json({ message: 'You must log in first.' });
  }

  const params = {
    '_id': req.session.userId,
  };
  User.findOne(params, (err, user) => {
    if (!user.subscriptionId) {
      return res.json({ 'message': 'Error fetching subscription' });
    }

    gateway.subscription.cancel(user.subscriptionId, (err, result) => {
      if (err) {
        return res.status(500).json({ 'message': 'Braintree: Error cancelling subscription.' });
      }

      user.subscriptionId = null;
      user.save(err => {
        if (err) return res.status(500).json({ 'message': 'Error cancelling subscription.' });
        res.status(200).json({});
      });
    });
  });
});

export default router;

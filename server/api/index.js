import express from 'express';

import braintree from './braintree';
import session from './session';

const router = express.Router();

router.use('/braintree', braintree);
router.use('/session', session);

export default router;

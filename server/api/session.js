import express from 'express';
import bcryptjs from 'bcryptjs';

import User from '../User';

const router = express.Router();

function createSession(req, res, next) {
  return (err, user) => {
    if (err) return next();
    req.session.userId = user._id;
    req.session.cookie.expires = true
    req.session.cookie.maxAge = 10000000000; // ~115 days
    return res.json(user);
  };
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

router.post('/register', function(req, res, next) {
  if (!req.body.email || !req.body.password) {
    res.status(500);
    return res.json({ 'message': 'You must fill out all of the items below.' });
  }

  if (!validateEmail(req.body.email)) {
    res.status(500);
    return res.json({ 'message': 'The email provided is not valid.' });
  }

  const params = {
    email: req.body.email,
  }

  User.findOne(params, (err, user) => {
    if (user) {
      res.status(500);
      return res.json({ 'message': 'This user already exists.' });
    }

    params['displayName'] = req.body.displayName;
    params['password'] = bcryptjs.hashSync(req.body.password, 10);
    params['level'] = req.body.level;

    User.create(params, createSession(req, res, next))
  });
});

router.post('/login', function(req, res, next) {
  if (!req.body.email || !req.body.password) {
    res.status(500);
    return res.json({ 'message': 'You must fill out all of the items below.' });
  }

  if (!validateEmail(req.body.email)) {
    res.status(500);
    return res.json({ 'message': 'The email provided is not valid.' });
  }

  const params = {
    email: req.body.email,
  }

  User.findOne(params, (err, user) => {
    if (!user) {
      res.status(500);
      return res.json({ 'message': 'Could not find a user with this email.' });
    }

    if (!bcryptjs.compareSync(req.body.password, user.password)) {
      res.status(500);
      return res.json({ 'message': 'The password provided doesn\'t match our records.' });
    }

    createSession(req, res, next)(null, user);
  })
});

router.post('/logout', function(req, res, next) {
  req.session.destroy(err => {
    if (err) return next(err);
    res.json({});
  });
});

router.get('/', (req, res, next) => {
  if (!req.session.userId) {
    return res.json({ '_id': null });
  }
  const query = {
    '_id': req.session.userId,
  };
  User.findOne(query, (err, user) => {
    if (err) return res.json({ '_id': null });
    res.json(user);
  });
});

export default router;

import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import emojic from 'emojic';
import express from 'express';
import fs from 'fs';
import session from 'express-session';
import morgan from 'morgan';
import mongoose from 'mongoose';

const config = require(`./config.${ process.env.NODE_ENV }.js`);
const routes = require('./routes');

const app = express();
const http = require('http').Server(app);

http.listen(config.PORT, good(`server: port ${ config.PORT }`))

mongoose.connect(config.DB_URL).then(
  good('mongodb >>' + config.DB_URL),
  bad('mongodb >>' + config.DB_URL)
);

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json({ 'limit': '25mb' }))
app.use(express.static('browser/dist'));
app.use(session({ secret: 'ls9d8ff' }));

app.use('/', routes);

function good(what) {
  return () => console.log(`${ emojic.thumbsUp }: ${ what }`);
}

function bad(what) {
  return () => console.log(`${ emojic.thumbsDown }: ${ what }`);
}

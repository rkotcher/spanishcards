import mongoose from 'mongoose';

export default mongoose.model('User', {
  email: String,
  level: String,
  password: String,
  displayName: String,
  subscriptionId: String,
});

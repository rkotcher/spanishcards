import express from 'express';
import api from './api';

const router = express.Router();

router.use('/api', api);

router.get('/*', function(req, res, next) {
  res.sendFile('index.html', {
    root: 'browser/dist',
  });
});

module.exports = router;

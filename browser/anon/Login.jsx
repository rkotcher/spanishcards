import _ from 'underscore';
import React from 'react';
import { browserHistory } from 'react-router';
import classnames from 'classnames';

const Login = React.createClass({
  getInitialState() {
    return {
      email: '',
      password: '',
      error: '',
    };
  },

  submit(e) {
    e.preventDefault();
    this.setState({ error: '' });

    $.ajax({
      type: 'POST',
      url: '/api/session/login',
      contentType: 'application/json',
      data: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      }),
      error: (request, status, error) => {
        this.setState({ error: request.responseJSON.message });
      },
      success: () => {
        browserHistory.push('/home');
      },
    });
  },

  render() {
    return (
      <form autoComplete='off' onSubmit={ this.submit }>
        <fieldset>
          {
            this.state.error ?
              <div className='alert alert-danger'>
                { this.state.error }
              </div>
            :
              null
          }
          <input style={{ display: 'none' }} type='text' name='somefakename' />
          <input style={{ display: 'none' }} type='password' name='anotherfakename' />
          <div className='form-group form-group-sm label-floating no-margin'>
            <label className='control-label'>Email</label>
            <input
              value={ this.state.email }
              onChange={ e => this.setState({ email: e.target.value }) }
              className='form-control'
              type='text'
            />
          </div>
          <div className='form-group form-group-sm label-floating no-margin'>
            <label className='control-label'>Password</label>
            <input
              value={ this.state.password }
              onChange={ e => this.setState({ password: e.target.value }) }
              className='form-control'
              type='password'
            />
          </div>
          <button className='btn btn-raised btn-sm btn-primary'>Login</button>
        </fieldset>
      </form>
    );
  },
});

const Register = React.createClass({
  getInitialState() {
    return {
      displayName: '',
      email: '',
      password: '',
      level: 'BEGINNER',
      error: '',
    };
  },

  submit(e) {
    e.preventDefault();
    this.setState({ error: '' });

    const user = {
      displayName: this.state.displayName,
      email: this.state.email,
      password: this.state.password,
      level: this.state.level,
    };

    $.ajax({
      type: 'POST',
      url: '/api/session/register',
      contentType: 'application/json',
      data: JSON.stringify(user),
    })
      .done(() => $.post('/api/braintree/customer'))
      .done(() => browserHistory.push('/home'))
      .fail(req => this.setState({ error: req.responseJSON.message }));
  },

  render() {
    return (
      <form autoComplete='off' method='post' onSubmit={ this.submit }>
        <fieldset>
          {
            this.state.error ?
              <div className='alert alert-danger'>
                { this.state.error }
              </div>
            :
              null
          }
          <input style={{ display: 'none' }} type='text' name='somefakename' />
          <input style={{ display: 'none' }} type='password' name='anotherfakename' />
          <div className='form-group form-group-sm label-floating no-margin'>
            <label className='control-label'>Email</label>
            <input
              value={ this.state.email }
              onChange={ e => this.setState({ email: e.target.value }) }
              className='form-control'
              type='text'
            />
          </div>
          <div className='form-group form-group-sm label-floating no-margin'>
            <label className='control-label'>Password</label>
            <input
              value={ this.state.password }
              onChange={ e => this.setState({ password: e.target.value }) }
              className='form-control'
              type='password'
            />
          </div>
          <div className='form-group form-group-sm label-floating no-margin'>
            <label className='control-label'>Display Name</label>
            <input
              value={ this.state.displayName }
              onChange={ e => this.setState({ displayName: e.target.value }) }
              className='form-control'
              type='text'
            />
          </div>
          <div className='form-group form-group-sm label-floating no-margin'>
            <label className='control-label'>Current level (you can change this later)</label>
            <select
              className='form-control'
              value={ this.state.level }
              onChange={ e => this.setState({ level: e.target.value }) }
            >
              <option value='BEGINNER'>BEGINNER</option>
              <option value='INTERMEDIATE'>INTERMEDIATE</option>
            </select>
          </div>
          <button className='btn btn-raised btn-sm btn-primary'>
            Register
          </button>
        </fieldset>
      </form>
    );
  },
});

const tabs = [{
  'name': 'Register',
  'component': Register,
},{
  'name': 'Login',
  'component': Login,
}];

export default React.createClass({
  getInitialState() {
    return {
      text: '',
      by: '',
      tab: tabs[0],
    };
  },

  render() {
    const Component = this.state.tab.component;

    return (
      <div id='login'>
        <div className='outer-container'>
          <div className='login-container panel panel-primary'>
            <ul className='nav nav-tabs custom-tab-styles'>
              {
                tabs.map((tab, index) => {
                  const classes = tab.name == this.state.tab.name ?
                    'underline' : '';

                  return (
                    <li key={ index }>
                      <a
                        className={ classes }
                        href='#'
                        onClick={ () => this.setState({ tab: tab }) }
                      >
                        { tab.name }
                      </a>
                    </li>
                  );
                })
              }
            </ul>
            <div className='panel-body content-container'>
              <Component sessionChanged={ this.props.sessionChanged }/>
            </div>
          </div>
        </div>
      </div>
    );
  },
});

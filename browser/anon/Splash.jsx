import React from 'react';
import Navbar from '../Navbar';
import { browserHistory } from 'react-router';

export default React.createClass({
  render() {
    return (
      <div className='splash-container'>
        <Navbar/>
        <div className='splash-content'>
          <h3>Logged-out page: Marketing material here</h3>
        </div>
      </div>
    );
  },
});

import React from 'react';
import { browserHistory } from 'react-router';

export default React.createClass({
  getInitialState() {
    return {
      isLoggedIn: false,
    };
  },

  componentDidMount() {
    $.get('/api/session')
      .then(session => this.setState({ isLoggedIn: !!session._id }));
  },

  home() {
    browserHistory.push('/home');
  },

  logout() {
    $.ajax({
      type: 'POST',
      url: '/api/session/logout',
      success: () => {
        browserHistory.push('/');
      },
    });
  },

  login() {
    browserHistory.push('/account');
  },

  subscribe() {
    browserHistory.push('/subscribe');
  },

  render() {
    return (
        <div className='navbar navbar-default'>
          <div className='container'>
            <div className='navbar-header'>
              <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
                <span className='icon-bar'></span>
                <span className='icon-bar'></span>
                <span className='icon-bar'></span>
              </button>
              <a className='navbar-brand' href='javascript:void(0)' onClick={ this.home }>Home</a>
            </div>
            <div className='navbar-collapse collapse navbar-responsive-collapse'>
              <ul className='nav navbar-nav navbar-right'>
                <li className='dropdown'>
                  <a href='bootstrap-elements.html' data-target='#' className='dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
                    <b className='caret'></b>
                    <div className='ripple-container'></div>
                  </a>
                  {
                    this.state.isLoggedIn ?
                      <ul className='dropdown-menu'>
                        <li><a href='javascript:void(0)' className='small-dropdown-item' onClick={ this.subscribe }>Subscribe</a></li>
                        <li><a href='javascript:void(0)' className='small-dropdown-item' onClick={ this.logout }>Log out</a></li>
                      </ul>
                    :
                      <ul className='dropdown-menu'>
                        <li><a href='javascript:void(0)' className='small-dropdown-item' onClick={ this.login }>Log in</a></li>
                      </ul>
                  }
                </li>
              </ul>
            </div>
          </div>
        </div>

    );
  },
});

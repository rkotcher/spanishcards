import React from 'react';
import { render } from 'react-dom';
import {
  Router,
  IndexRoute,
  Route,
  browserHistory
} from 'react-router';

import Login from './anon/Login';
import Home from './home/Home';
import Splash from './anon/Splash';
import Payment from './payment/Payment';

function redirectIfSession(nextState, replace, callback) {
  $.get('/api/session').then(session => {
    session._id && replace('/home');
    callback();
  });
}

function checkSession(nextState, replace, callback) {
  $.get('/api/session').then(session => {
    !session._id && replace('/');
    callback();
  });
}

const router = (
  <Router history={ browserHistory }>
    <Route path='/'>
      <IndexRoute component={ Splash } onEnter={ redirectIfSession } />
      <Route path='account' component={ Login } onEnter={ redirectIfSession } />
      <Route path='subscribe' component={ Payment } onEnter={ checkSession }/>
      <Route path='home' component={ Home } onEnter={ checkSession } />
    </Route>
  </Router>
);

$(() => {
  $.material.init();
  render(router, document.getElementById('root'));
})

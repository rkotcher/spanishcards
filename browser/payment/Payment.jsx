import $ from 'jquery';
import _ from 'underscore';
import React from 'react';
import { browserHistory } from 'react-router';
import classnames from 'classnames';
import Navbar from '../Navbar'
import { BarLoader } from 'react-spinners';

import dropin from 'braintree-web-drop-in';

export default React.createClass({
  getInitialState() {
    return {
      loading: true,
      subscription: null,
      showSubmitButton: false,
    };
  },

  setupDropin() {
    $.get('/api/braintree/client_token').then(token => {
      const options = {
        container: '#dropin-container',
        authorization: token,
      };

      dropin.create(options, (err, instance) => {
        if (err) return console.error(err);

        this.setState({ showSubmitButton: true, loading: false }, () => {
          $('#submit-button').on('click', () => {
            instance.requestPaymentMethod((err, payload) => {
              if (err) return console.error(err);

              this.setState({ loading: true });

              $.ajax({
                url: '/api/braintree/subscription',
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify({
                  payment_method_nonce: payload.nonce,
                }),
                success: () => {
                  browserHistory.push('/home');
                },
                error: () => {
                  console.error('Did not successfully subscribe :(');
                },
                always: () => {
                  this.setState({ loading: false });
                },
              })
            });
          });
        });
      });
    });
  },

  componentDidMount() {
    $.get('/api/braintree/subscription').then(response => {
      this.setState({ subscription: response.subscription }, () => {
        !this.state.subscription && this.setupDropin();
        this.state.subscription && this.setState({ loading: false });
      });
    });
  },

  cancel() {
    this.setState({ loading: true });

    $.ajax({
      url: '/api/braintree/subscription',
      type: 'DELETE',
      success: () => {
        browserHistory.push('/home');
      },
      error: err => console.log(err),
    }).always(() => this.setState({ loading: false }));
  },

  render() {
    return (
      <div>
        <Navbar/>
        <BarLoader loading={ this.state.loading } color='#0000ff' width='100%'/>
        <div className='payment-page-container'>
          {
            this.state.subscription ?
              <div>
                <div className='your-subscription'>Your Subscription</div>
                <div className='subscription-container'>
                  <p>
                    <strong>Lookup ID:</strong> { this.state.subscription.id }
                  </p>
                  <p>
                    <strong>Plan ID:</strong> { this.state.subscription.planId }
                  </p>
                  <p>
                    <strong>Created:</strong> { new String(new Date(this.state.subscription.createdAt)) }
                  </p>
                  <a href='#' onClick={ this.cancel }>Cancel subscription</a>
                </div>
              </div>
            :
              <div className='payment-items-container'>
                <div id='dropin-container'/>
                {
                  this.state.showSubmitButton ?
                    <button id='submit-button'>Subscribe</button>
                  :
                    null
                }
              </div>
          }
        </div>
      </div>
    );
  },
});

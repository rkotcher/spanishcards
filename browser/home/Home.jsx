import _ from 'underscore';
import classnames from 'classnames';
import React from 'react';
import Navbar from '../Navbar'

export default React.createClass({
  componentDidMount() {

  },

  render() {
    return (
      <div className='home-container'>
        <Navbar/>

        <div className='home-content'>
          <h3>Logged-in page: App stuff goes here</h3>
        </div>
      </div>
    );
  },
});
